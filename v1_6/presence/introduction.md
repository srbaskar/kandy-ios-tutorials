## Presence

Presence service enables you to retrieve the "last seen" date of contacts.