### VCard Helper Methods

KandySDK provides you with an easy way to create a VCard by KandyContact and vice versa.

#### VCard by KandyContact

```objectivec
-(void)getVCardForKandyContact:(id<KandyContactProtocol>)kandyContact {
  [[Kandy sharedInstance].services.contacts createVCardDataByContact:kandyContact completionBlock:^(NSError *error, NSData *vCardData) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```

#### KandyContact by VCard

`````objectivec
-(id<KandyContactProtocol>)getKandyContactByVCard:( NSData*)vCardData {
  NSError* error = nil;
  [[Kandy sharedInstance].services.contacts createContactByVCard:vCardData error:&error];
  if (error) {
    // Failure
  } else {
    // Success
  }
}
```
