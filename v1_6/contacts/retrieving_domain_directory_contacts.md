### Retrieving Domain Directory Contacts

The following method returns the domain directory contacts as id\<KandyContactProtocol\>; objects. If the operation failed, it returns an error.

```objectivec
-(void)getDomainDirectoryContacts {
  [[Kandy sharedInstance].services.contacts getDomainDirectoryContactsWithResponseCallback:^(NSError *error, NSArray *kandyContacts) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
