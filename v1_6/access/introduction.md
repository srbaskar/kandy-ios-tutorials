## Access

After you have provisioned a user, you should log the user into the system.

Login is required for access to the SDK capabilities, i.e. Calls, Chats, Presence, etc.