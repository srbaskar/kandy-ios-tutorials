### Background Execution

VOIP applications are allowed to request CPU while in background by calling the setKeepAliveTimeout. You should call Kandy’s handler inside.

```objectivec
- (void)applicationWillResignActive:(UIApplication *)application { BOOL backgroundAccepted = [[UIApplication sharedApplication]setKeepAliveTimeout:600 handler:^{
[[Kandy sharedInstance].access keepAliveTimeoutHandler];
}];
}
```
