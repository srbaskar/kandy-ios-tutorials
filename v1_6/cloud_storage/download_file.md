## Download File

To download file from the cloud storage, use the following code:

```objectivec
-(IBAction)downloadFile:(id)sender{
  NSString * downloadFilename = <Path and file name>;
  id<KandyFileItemProtocol> transferingFileItem = [KandyMessageBuilder createFileItem:nil text:nil];
  NSString * contentUUID = <The file ContentUUID you wish to download>;
  [transferingFileItem updateContentUUID:contentUUID];
    // Download the file
  [[Kandy sharedInstance].services.cloudStorage downloadMedia:transferingFileItem fileName:[downloadFileName lastPathComponent] downloadPath:self.documentsDirectory progressCallback:^(KandyTransferProgress *transferProgress) {
      // Notify UI about progress change
    } responseCallback:^(NSError *error, NSString *filePath) {
    if (error) {
      // Fail
    } else {
      // Success. transferingFileItem now filled with all relevant information
    }
  }];
}
```
