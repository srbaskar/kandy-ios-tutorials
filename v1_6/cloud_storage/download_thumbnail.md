## Download thumbnail

To download media thumbnail from the cloud storage, use the following code (thumbnails only available for images and video files):

```objectivec
-(IBAction)downloadFile:(id)sender{
  NSString * thumbnailFilename = <Path and file name>;
  id<KandyFileItemProtocol> fileItem = [KandyMessageBuilder createFileItem:thumbnailFileName text:nil];
  NSString * contentUUID = < The file ContentUUID you wish to download its thumbnail>;
  [transferingFileItem updateContentUUID:contentUUID];
  // Download the file thumbnail
  [[Kandy sharedInstance].services.cloudStorage downloadThumbnail:fileItem thumbnailName:[ thumbnailFilename lastPathComponent] downloadPath:self.documentsDirectory thumbnailSize:EKandyThumbnailSize_medium progressCallback:^(KandyTransferProgress *transferProgress) {
    // Notify UI about progress change
  } responseCallback:^(NSError *error, NSString *filePath) {
    if (error) {
      // Fail
    } else {
      // Success
    }
  }];
}
```
