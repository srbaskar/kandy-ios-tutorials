## Upload File

To upload file to the cloud storage use the following code:

```objectivec
-(IBAction)uploadFile:(id)sender{
// Get file name and path for selected file
id<KandyFileItemProtocol> fileItem = [KandyMessageBuilder createFileItem:@”localPathFile” text:nil];

// Upload the file
[[Kandy sharedInstance].services.cloudStorage uploadMedia: fileItem progressCallback:^(KandyTransferProgress *transferProgress) {
  // Notify UI about progress change
  } responseCallback:^(NSError *error) {
    if (error) {
        // Fail
    } else {
        // Success. Now you can get ContentUUID from updated fileItem to download your file later
    }
  }];
}
```
