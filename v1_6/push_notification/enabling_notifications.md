### Enabling Notifications

Once you have completed the preconditions above, you can handle the notifications on app launch.

To enable Kandy Remote Notifications.

Once the user is logged in, you can enable Kandy push notifications using the following code:

```objectivec
[[Kandy sharedInstance].access login:kandyUserInfo andResponseCallback:^(NSError *error) {
  if (!error) {
    NSData* deviceToken = [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceToken"];
    NSString* bundleId = [[NSBundle mainBundle]bundleIdentifier];
    [[Kandy sharedInstance].services.push enableRemoteNotificationsWithToken:deviceToken bundleId:bundleId isSandbox:isSandbox responseCallback:^(NSError *error) {
      if (error) {
        //handle error e.g no Internet connection
      }
    }];
  }
}];
```

enableRemoteNotificationsWithToken: will pass the deviceToken to Kandy cloud and will later be used by it to refer push notification to this device.