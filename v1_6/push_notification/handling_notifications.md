### Handling Notifications

After the users are logged in, they can handle Kandy notifications.

To handle Kandy SDK remote notification on the application launch, use the following code:

```objectivec
-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  //…
  //initialize the KandySDK
  //register user notification settings
  if (launchOptions && [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey] != nil) {
    NSDictionary* remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    id<KandyEventProtocol> event = [[Kandy sharedInstance].notifications getRemoteNotificationEvent:remoteNotification];
    [self_handleEventUI:event]; // event is nil if not handled by Kandy
    [[Kandy sharedInstance].notifications handleRemoteNotification:remoteNotification
    responseCallback: ^(NSError *error){
      if (error &&
        error.domain isEqualToString:kandyNotificationServiceErrorDomian] &&
        error.code == EKandyNotificationServiceError_pushFormatNotSupported) {
        // Could not handle remote notification
      } else {
        // Success
      }
    }];
  }
}
```

To handle Kandy SDK remote notification while application is running, use the following code:

```objectivec
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  [[Kandy sharedInstance].services.push handleRemoteNotification:userInfo responseCallback:^(NSError *error) {
    if (error &&
       [error.domain isEqualToString:KandyNotificationServiceErrorDomain] &&
       error.code == EKandyNotificationServiceError_pushFormatNotSupporte) {
      //Push format not supported by Kandy, handle the notification my self
    }
  }];
}
```

If you wish to get the event prior to connecting (perhaps for UI changes), you can also use the call:

```objectivec
id<KandyEventProtocol> event = [[Kandy sharedInstance].services.push getRemoteNotificationEvent:remoteNotification];
```
