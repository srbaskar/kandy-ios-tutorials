### Request Activation Code

This call will send a registration request to the server.

The verification method (currently supports only SMS) sends a validation code to the user, which the user should enter on the "validate" method.

_**Note:** You can receive the KandyAreaCode by MCC/IP using the location service using getCountryInfo API call._

```objectivec
-(void)registerSubscriber{
  KandyAreaCode * areaCode = [[KandyAreaCode alloc] initWithISOCode:@"US" andCountryName:@"United States" andPhonePrefix:@"+1"];
  NSString * strPhoneNumber = @"<phone number>";
  [[Kandy sharedInstance].provisioning requestCode:areaCode phoneNumber:strPhoneNumber codeRetrivalMethod:EKandyValidationMethod_sms responseCallback:^(NSError *error, NSString *destinationToValidate) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
