### Pull Events

To pull pending Chat events from the server, use the following code:

```objectivec
-(void)pullEvents {
  [[Kandy sharedInstance].services.chat pullEventsWithResponseCallback:^(NSError *error, NSArray *kandyEvents) {
    if (error) {
      //Failure
    }
    else {
      //Success
    }
  }];
}
```
