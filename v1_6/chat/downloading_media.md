### Downloading Media

To download media, use the following:

```objectivec
-(void)downloadMediaForMessage:( id<KandyMessageProtocol>)kandyMessage {
  [[Kandy sharedInstance].services.chat downloadMedia:kandyMessage progressCallback:^(KandyTransferProgress *transferProgress) {
    //Download progress
  } responseCallback:^(NSError *error, NSString *fileAbsolutePath) {
    if (error) {
      //Failure
    } else {
      //Success
    }
  }];
}
```
