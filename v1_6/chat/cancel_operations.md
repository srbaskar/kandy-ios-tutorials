### Cancel Operation

You can cancel an upload/download operation associated to a Kandy Message using:

```objectivec
-(void)cancelOperation:( id<KandyMessageProtocol>)kandyMessage {
  [[Kandy sharedInstance].services.chat cancelWithKandyMessage:kandyMessage responseCallback:^(NSError *error) {
    if (error) {
      //Failure
    } else {
      //Success
    }
  }];
}
```
