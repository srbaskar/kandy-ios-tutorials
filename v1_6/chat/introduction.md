## Chat

Chat service enables you to send/receive text and rich media to/from single or group recipient.

Make sure to enable the Push Notification to notify you of new messages or events, even when you are not actively using your application.