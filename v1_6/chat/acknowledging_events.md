### Acknowledging Events

You should respond to KandyChatServiceNotificationDelegate Chat notifications by acknowledging them, otherwise, as mentioned before, you will receive them again.

To do this, use the following code:

* For Chat received:

  ```objectivec
  -(void)onMessageReceived:(KandyMessage*)kandyMessage {
    [kandyMessage markAsReceivedWithResponseCallback:^(NSError *error) {
        if (error) {
          //Failure
        } else {
          //Success
        }
    }];
  }
  ```

* For Chat delivered:

  ```objectivec
  -(void)onMessageDelivered:(KandyDeliveryAck*)ackData{
    [[Kandy sharedInstance].services.chat markAsReceived:ackData withResponseCallback:^(NSError *error) {
      if (error) {
        //Failure
      } else {
        //Success
      }
    }
  }];
  ```
