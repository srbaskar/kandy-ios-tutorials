### Send Rich Media Message

To send an image message use the following code:

```objectivec
-(void)sendImageMsg {
  KandyRecord * kandyRecord = [[KandyRecord alloc] initWithURI:@"DestinationURI"];
  id<KandyMediaItemProtocol> mediaItem = [KandyMessageBuilder createImageItem:@"absolutePathToImageFile text:@"Optional text"];
  KandyChatMessage *message = [[KandyChatMessage alloc] initWithMediaItem:mediaItem recipient:kandyRecord];
  [[Kandy sharedInstance].services.chat sendChat:chatMessage progressCallback:^(KandyTransferProgress *transferProgress){
    //progress
  }
  responseCallback:^(NSError *error) {
    if (error) {
      //Failure
    } else {
      //Success
    }
  }];
}
```

For other media messages the flow is similar, just create the appropriate media item:

* Video:

  ```objectivec
  id<KandyMediaItemProtocol> mediaItem = [KandyMessageBuilder createVideoItem:@"absolutePathToImageFile" text:@"Optional text"];
  ```

* Audio:

  ```objectivec
  id<KandyMediaItemProtocol> mediaItem = [KandyMessageBuilder createAudioItem:@"absolutePathToImageFile" text:@"Optional text"];
  ```

* Location:

  ```objectivec
  CLLocation *location = [[CLLocation alloc] initWithLatitude:40.8283018 longitude:16.5500004];
  id<KandyMediaItemProtocol> mediaItem = [KandyMessageBuilder createLocationItem:location text:@"Optional text"];
  ```

* Contact:

  ```objectivec
  [[Kandy sharedInstance].services.contacts createVCardDataByContact:kandyContact completionBlock:^(NSError *error, NSData *vCardData) {
    if (!error) {
      NSString *vcardPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"vcard.vcf"];
      [vCardData writeToFile:vcardPath atomically:YES];
      id<KandyMediaItemProtocol> contactMediaItem = [KandyMessageBuilder createContactItem:vcardPath text:@"Optional text];
    }
  }];
  ```
