### Notifications

```objectivec
-(void)onGroupDestroyed:(KandyGroupDestroyed*)groupDestroyedEvent{
  //your code here
}

-(void)onGroupUpdated:(KandyGroupUpdated*)groupUpdatedEvent{
  //your code here
}

-(void)onParticipantJoined:(KandyGroupParticipantJoined*) groupParticipantJoinedEvent{
  //your code here
}

-(void)onParticipantKicked:(KandyGroupParticipantKicked*) groupParticipantKickedEvent{
  //your code here
}

-(void)onParticipantLeft:(KandyGroupParticipantLeft*) groupParticipantLeftEvent{
  //your code here
}
```
