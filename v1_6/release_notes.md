## Release Notes 1.6

### New Features:
* Subscriber Provisioning
  * Added support for sending OTP in different methods

* Contacts Service
  * Support for personal address book – enables you to create custom address book


* Call Service
  * New KandyCallServiceSettings
  * Support for transfer call (experimental)
  * Support for calls with blocked caller ID
  * Support for default camera position when initiating / getting a video call (configurable via KandyCallServiceSettings)
  * Support for disabling camera orientation  change in video calls (configurable via KandyCallServiceSettings)
  * New call state – “answering“ (EKandyCallState_answering)
  * KandySDK now able to reject incoming calls incase already in GSM call (configurable via KandyCallServiceSettings)

* Chat Service
  * Support for messages with custom data
  * Added support for sending/receiving messages with file attachment

* Cloud Storage Service
  * New service added – you can now upload / download any kind of file

* Billing Service
  * New service added


### Breaking Changes:
  * Access
    * Alongside with [[Kandy sharedInstance].access.connectionState where is a new state parameter: [[Kandy sharedInstance].access.registrationState

  * Call
    * Method changed - createVoipCall:callee:options: - options parameter takes new type EKandyOutgingVoIPCallOptions
    * Method - createPSTNCall:destination: - now takes new parameter options (EKandyOutgingPSTNCallOptions)

  * Address Book Service
    * updatePersonalConatct – added parameter contactID (NSString)
    * deletePersonalConatct – takes now contactID (NSString*) instead of params (KandyContactParams*)

  * Subscriber Provisioning
    * Method validateDomain:andDomainSecret:responseCallback: renamed to validateDomain:domainSecret:responseCallback:
    * Method requestCode:phoneNumber:responseCallback: now takes new parameter codeRetrivalMethod (EKandyValidationMethod)

  * Location Service
    * reportLocation: status: responseCallback: now takes locationParams (KandyLocationParams*) instead of location (CLLocation*)

  * Chat Service
    * Property removed from KandyChatService - messageBuilder (KandyMessageBuilder*)Use static methods from KandyMessageBuilder class

  * Contact Service
    * KandyContactsService method updatePersonalConatct:contactID:responseCallback: typo fixed updatePersonalContact:contactID: responseCallback:
    * KandyContactsService method deletePersonalContact:contactID:responseCallback: typo fixed deletePersonalContact:contactID: responseCallback:
