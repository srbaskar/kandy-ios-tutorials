## Notifications

You can receive Contacts-related notifications (onDeviceContatcsChanged).

Implement KandyContactsServiceNotificationDelegate with its suitable classes.

```objectivec
-(void)registerForContactsChanges{
  [[Kandy sharedInstance].services.contacts registerNotifications:self];
}
```
