## Last Seen

To retrieve the last seen date, call the method:

```objectivec
retrievePresenceForRecords:withResponseCallback
```

To retrieve \<KandyPresenceProtocol\> objects representing the presence data, including last seen date, of the requested array of contacts in the form of KandyRecords.

The response callback also contains missingPresenceKandyRecords —an array with the requested KandyRecords for which no presence data was found.

```objectivec
- (void)getLastSeen{
  KandyRecord* kandyRecord = [[KandyRecord alloc] initWithURI:@"Contact@DomainName.Com" type:EKandyRecordType_contact];

  [[Kandy sharedInstance].services.presence getPresenceForRecords:[NSArray arrayWithObject:kandyRecord] responseCallback:^(NSError *error, NSArray *presenceObjects, NSArray *missingPresenceKandyRecords) {
      if (error) {
        // Failure
      } else {
        // Success
        id <KandyPresenceProtocol> presenceObject = [presenceObjects objectAtIndex:0];
        NSString *strDate = [NSDateFormatter localizedStringFromDate:presenceObject.lastSeen
        dateStyle:NSDateFormatterShortStyle
        timeStyle:NSDateFormatterShortStyle];
        self.lblLastSeen.text = strDate;
     }
  }];
}
```
