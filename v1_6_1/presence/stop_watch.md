## Stop Watch Presence

Stop watching other user’s presence state

```objectivec
stopWatchPresenceForRecords:responseCallback:
```

```objectivec
- (void)stopWatch
 {
    KandyRecord* kandyRecord = [[KandyRecord alloc] initWithURI:user_id type:EKandyRecordType_contact];
   
 [[Kandy sharedInstance].services.presence startWatchPresenceForRecords:[NSArray arrayWithObject:kandyRecord] responseCallback:^(NSError *error) {
        UIAlertView *alert;
        
        if (error) {
		      //start watch failed
		  } else {
            //start watch succeeded
        }
	}];
}

```
