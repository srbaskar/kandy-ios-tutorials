## Notifications

You can be notified on call delegate methods: gotIncomingCall, callStateChanged, gotMissedCall, …

In order to do so, implement the KandyCallServiceNotificationDelegate:

```objectivec
-(void)registerForConnectEvents{
  [[Kandy sharedInstance].services.call registerServiceNotificationsDelegate:self];
}
```

then use the next code:

```objectivec
-(void) gotIncomingCall:(id<KandyIncomingCallProtocol>)call
{
  BOOL isAnswerWithVideo = YES;
  [call accept:isAnswerWithVideo withResponseBlock:^(NSError *error) {
    if (error) {
      //Failure
    }
    else {
      //Success
    }
  }];
}
```

You will also be notified about audio output changes:

```objectivec
-(void) availableAudioOutputChanged:(NSArray*) updatedAvailableAudioOutputs {

}
```
