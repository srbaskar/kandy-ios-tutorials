## Call Statistics

KandyCallProtocol lets you retrieve information about quality of the connection. 

To get statistics for specific call use following method:

```
{

	id<KandyCallProtocol> call = <Call object>;
	[call getRTPStatisticsWithCompletion:^(id<KandyCallRTPStatisticsProtocol> statistics, NSError *error) {
		// Handle statistics information
	}
}
```

