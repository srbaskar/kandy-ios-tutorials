### Call Settings

Call settings represented by KandyCallServiceSettings class.

* defaultCameraPosition: The default camera position when initiating video (on call start or during the call). Default value is EKandyCameraPosition_front
* cameraOrientationSupport: The camera orientation support (while on a video call). Default value is EKandyCameraOrientationSupport_statusBar
* shouldRejectIncomingCallsWhileInGSMCall: Should the incoming call be automatically rejected incase already in GSM call. Default value is YES
