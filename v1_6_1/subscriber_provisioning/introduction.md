# Subscriber Provisioning

The subscriber provisioning lets you add/remove users to/from the system.

You can add subscribers in two ways:

* Admin — can create (multiple) subscribers using the web GUI.
* Users — can create individual subscribers using the SDK. Currently, the SDK supports adding subscribers using their phone number.

Subscription flow using phone number:

* Send validation code using the required phone number
* The user copies the validation code manually from the SMS
* Send the validation code with the related phone number to the SDK
* Get response with the KandyUserInfo that contains the relevant data for connecting the system
