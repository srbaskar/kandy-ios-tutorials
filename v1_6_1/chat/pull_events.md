## Pull Events

To pull pending Chat events from the server, use the following code:

```objectivec
-(void)pullEvents {
    [[Kandy sharedInstance].services.chat pullEventsWithResponseCallback:^(NSError *error) {
        if (error) {
          // error
        }
    }];
}
```
