## Attaching custom data to message

Chat messages can be send with the additional custom data. To attach your data to message create chat message and update additional data for its mediaItem:

```objectivec

KandyRecord * kandyRecord = [[KandyRecord alloc] initWithURI:@"DestinationURI"];
// Create chat message. Custom user’s data can be added to any type of chat messages
KandyChatMessage *textMessage = [[KandyChatMessage alloc] initWithText:@”Message text” recipient:kandyRecord];
// Add custom data to message
[textMessage.mediaItem updateAddtitionalData:@{@"timestamp": @( [NDate timeIntervalSinceReferenceDate])}];

```
