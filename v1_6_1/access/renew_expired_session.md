## Renew Expired Session

Your session may expire over time. If so, you will be notified about it via Access notifications.

You can renew your expired session using:

```objectivec
[[Kandy sharedInstance].access renewExpiredSession: :^(NSError *error) {
  if (error) {
    // Failure
  } else {
    // Success
  }
}];
```
