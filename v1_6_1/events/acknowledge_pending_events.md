## Acknowledging Pending Events

You should mark pulled pending events as received by acknowledging them, otherwise you will receive them again and again every time you pull pending events from server.
To flag event as received and stop getting it in the future, use markAsReceivedWithResponseCallback: method from KandyEventProtocol:

```
-(void)onMessageReceived:(KandyMessage*)kandyMessage {
    [kandyMessage markAsReceivedWithResponseCallback:^(NSError *error) {
        if(error){
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```

You can also use markEventsAsReceived:responseCallback: method of KandyEventService to mark single event or multiple events at once:

### For chat message:

```
-(void)onMessageReceived:(KandyMessage*)kandyMessage {
    [[Kandy sharedInstance].services.events markEventsAsReceived:@[kandyMessage] responseCallback:^(NSError *error) {
        if(error){
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```

### For delivery notification:

```
-(void)onMessageDelivered:(KandyDeliveryAck*)ackData{
    [[Kandy sharedInstance].services.events markEventsAsReceived: @[ackData] responseCallback:^(NSError *error){
        if(error)
        {
            //Failure
        }
        else
        {
            //Success
        }

    }];
}


```
