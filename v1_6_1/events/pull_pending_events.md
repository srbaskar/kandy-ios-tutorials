## Pull Pending Events

To pull pending events from the server, use the following code:

Events pulled will be dispatched to relevant service delegates, and will be available via Chat/Group/Call services.

```
-(void)pullEvents{
    [[Kandy sharedInstance].services.events pullPendingEventsWithResponseCallback:^(NSError *error, NSArray *kandyEvents) {
        if(error)
        {
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```
