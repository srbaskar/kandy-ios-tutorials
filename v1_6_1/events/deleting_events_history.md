## Deleting Events from History 

KandyEventService allows to delete events from history. There are 3 options available:

1.Delete all events and conversations from history for current provisioned user. 

***Code sample:***

```objective-c

-(void)deleteAllConversations 
{
    [[Kandy sharedInstance].services.events deleteAllConversationsWithResponseCallback:^(NSError * error) {
        if(error)
        {
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```

2.Delete conversations of current provisioned user with specified remote destinations. Takes array of KandyRecord which can be either user or group. This will delete all events from history for specified destinations. 

***Code sample:***

```objective-c

-(void)deleteConversations:(NSArray<KandyRecord*>*)destinations
{
    [[Kandy sharedInstance].services.events deleteConversations:destinations responseCallback:^(NSError * error) {
        if(error)
        {
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```

3.Delete specific events from the conversation of current provisioned user and specified remote destination which can be either user or group.

***Code sample:***

```objective-c

- (void)deleteHistoryEvents:(NSArray<NSString*>*)eventUUIDs forDestination:(KandyRecord*)destination
{
    [[Kandy sharedInstance].services.events deleteHistoryEvents:eventUUIDs destination:destination responseCallback:^(NSError * error) {
        if(error)
        {
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```
