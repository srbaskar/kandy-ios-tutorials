## Step 5. Start coding!

The SDK is now installed and setup. You can use the SDK from any of your implementation files by adding the Kandy SDK header file:

```objectivec
(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [Kandy initializeWithDomainKey:@"YourDomainKey" domainSecret:@"YourDomainSecret"];
  return YES;
}
```
