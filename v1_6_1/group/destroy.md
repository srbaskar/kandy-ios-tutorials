## Group Destroy

```objectivec
-(void)destroyGroup{
  [[Kandy sharedInstance].services.group destroyGroup:self.kandyGroup.groupId responseCallback:^(NSError *error) {
    if (error) {
      //error
    } else {
      //group destryed
    }
  }];
}
```
