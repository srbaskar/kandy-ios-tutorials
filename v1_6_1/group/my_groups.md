## My Groups

To get all groups associated with a KandyRecord call this method:

```objectivec
-(void)getMyGroups{
  [[Kandy sharedInstance].services.group getMyGroupsWithResponseCallback:^(NSError *error, NSArray *groups) {
    if (error) {
       //error
    } else {
      /NSLog(@”My groups: %@”,groups);
    }
  }];
}
```
