# Group

In addition to messaging group KandyRecords (chat service), the Group service provides a way to manage your groups and keep you updated with relevant group changes.

Make sure to enable the Push Notification to notify you of new messages or events, even when you are not actively using your application.
