# Cancel Media Transfer

To cancel file upload/download process use the following code:

```objectivec
-(IBAction)cancelTransfer:(id)sender{
  [[Kandy sharedInstance].services.cloudStorage cancelMediaTransfer:self.transferingFileItem responseCallback:^(NSError *error) {
    if (error) {
      // Fail
    } else {
      // Success
    }
  }];
}
```
