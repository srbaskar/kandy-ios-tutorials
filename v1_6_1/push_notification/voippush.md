## VOIP Push

Starting from iOS 8.1 there is another push type available - VoIP Push. VoIP Push provides additional functionality like running app in background and provide CPU resources so the app can prepare itself to handle push event and notify the user. You can find more information about VoIP Push and PushKit framework on Apple Developer Portal: [Introduction to PushKit](https://developer.apple.com/library/prerelease/ios/documentation/NetworkingInternet/Reference/PushKit_Framework/index.html). Before registering VoIP Push token with KandySDK you should follow the steps as described in 7.1 but create VoIP Push certificate instead of standard one.

Following steps will show you sample usage of PushKit:

* Create class implementing PKPushRegistryDelegate protocol.
* When your app is ready to receive VoIP Push notifications, initialize PKPushRegistry object:

  ```objectivec
  PKPushRegistry * pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
		pushRegistry.delegate = self;
    	pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
  ```

* Implement  PKPushRegistryDelegate methods:

  ```objectivec
  -(void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type
	{
        		// Registered for VoIP push notifications. Store push token for future use
		[[NSUserDefaults standardUserDefaults]setObject:credentials.token forKey:@”voipToken”];
	}
	-(void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type
	{
    		// Received VoIP Push Notification, handle it
	}
	-(void)pushRegistry:(PKPushRegistry *)registry didInvalidatePushTokenForType:(NSString *)type {
    		// VoIP Push credentials invalidated
    		NSString* bundleId = [[NSBundle mainBundle]bundleIdentifier]
    		[[Kandy sharedInstance].services.push disableRemoteNotificationsWithBundleId: bundleId responseCallback:^(NSErro *error) {
        if (error) {
				// Handle error
	        }
    	}];
    }

  ```
